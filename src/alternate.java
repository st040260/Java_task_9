import java.util.Iterator;
import java.util.stream.Stream;

public class alternate {
    public static Stream zip(Stream first, Stream second){
        Stream.Builder<Long> tmpResult = Stream.builder();

        Iterator FirstIterator = first.iterator();
        Iterator SecondIterator = second.iterator();

        while (true){
            if(!(FirstIterator.hasNext()&&SecondIterator.hasNext())) break;
            else {
                tmpResult.accept((Long) FirstIterator.next());
                tmpResult.accept((Long) SecondIterator.next());
            }
        }
        Stream<Long> result = tmpResult.build();
        return result;
    }
}
