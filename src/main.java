import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class main {
    public static void main(String[] args) {
        ArrayList<Long> firstStreamIni = new ArrayList<Long>();
        ArrayList<Long> seconndStreamIni = new ArrayList<Long>();

        for (int i = 0; i < 10; i++) firstStreamIni.add((long) i);
        for (int i = 10; i < 25; i++) seconndStreamIni.add((long) i);

        Stream<Long> firstStream = firstStreamIni.stream();
        Stream<Long> seconndStream = seconndStreamIni.stream();
        Stream<Long> result = alternate.zip(firstStream, seconndStream);
        List<Long> collect = result
                .collect(Collectors.toList());
        System.out.println(collect);
    }
}
